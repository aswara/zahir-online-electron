// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import routes from '../constants/routes';
import styles from './Home.css';
import logo from '../assets/zahir.svg';
import illustration from '../assets/illustration.svg';
import infoIcon from '../assets/icon-info.svg';
import { shell, remote } from 'electron';


export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      IP: null
    };
  }

  componentDidMount() {

      //get IP Local
      window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;  
      const pc = new RTCPeerConnection({iceServers:[]}), 
      noop = function(){}; 
     
      pc.createDataChannel("");  
      pc.createOffer(pc.setLocalDescription.bind(pc), noop);   
      pc.onicecandidate = (ice) => { 
          if(!ice || !ice.candidate || !ice.candidate.candidate)  return;
          var myIP = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(ice.candidate.candidate)[1];

          if(myIP) {
            this.setState({
              IP: myIP
            })
          }
      }
  }

  openBrowser = (url) => {
  	shell.openExternal(url)
  }

  handleOpen = (url) => {
    shell.openExternal(url)
    remote.app.exit();
  }

  render() {
    const { IP } = this.state

    return (
      <div>
        <div className={styles.container} data-tid="container">
          <div className={styles.header}>
            <img src={logo} />
          </div>
          <div>
            <div className={styles.image}>
              <img src={illustration} />
            </div>
            <div className={styles.content}>
              <div>
                Recommended use <span>Google Chrome</span> to run this application.{' '}
                <a href="#" onClick={()=> { this.openBrowser("https://www.google.com/chrome") }}>Download</a>
              </div>
              <p>
                Web server IP address: <span>{IP ? IP : ''}</span>
              </p>
              <div>
                Zahir Online URL: <a href="#">{IP ? `http://${IP}:8989` : ''}</a>
              </div>
              <button onClick={()=> { this.openBrowser(`http://${IP}:8989`) }} className={styles.btn}>OPEN</button>
            </div>
          </div>
          <div className={styles.footer}>
            <div>
              <img
                className={styles.info}
                src={infoIcon}
                onClick={()=>{ this.openBrowser('https://www.zahironline.com') }}
                />
            </div>
            <div className={styles.copyright}>
              &copy; 2015-2019, PT. Zahir International
            </div>
          </div>
        </div>
      </div>
    );
  }
}
